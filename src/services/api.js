const urlPath = 'http://localhost:8080/';

export async function getAllStops() {
  try {
    const response = await fetch(urlPath + 'stops');
    if (!response.ok) {
      throw new Error(`Error fetching data: ${response.status}`);
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(`Error: ${error.message}`);
  }
}

export async function getStopDetailsByName(stopName, direction, type) {
  try {
    const url = urlPath + 'stops';
    const body = JSON.stringify({ stopName, direction, type });

    const response = await fetch(url, {
      method: 'POST', // You may want to use 'GET' or 'PUT' based on your API endpoint requirements
      headers: {
        'Content-Type': 'application/json',
        // Add any other headers if needed
      },
      body: body,
    });

    if (!response.ok) {
      throw new Error(`Error fetching data: ${response.status}`);
    }

    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(`Error: ${error.message}`);
  }
}


export async function getAllShapes() {
  try {
    const response = await fetch(urlPath + 'shapes');
    if (!response.ok) {
      throw new Error(`Error fetching data: ${response.status}`);
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(`Error: ${error.message}`);
  }
}
