import { createApp } from 'vue'
import App from './App.vue'

// UI LIBRARY
import PrimeVue from 'primevue/config';
import 'primevue/resources/themes/lara-light-green/theme.css'

// UI COMPONENT
import Dialog from 'primevue/dialog';
import Button from "primevue/button"
import SelectButton from 'primevue/selectbutton';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';


const app = createApp(App);
app.use(PrimeVue);

// UI COMPONENT INJECTOR
app.component('Dialog', Dialog);
app.component('Button', Button);
app.component('SelectButton', SelectButton);
app.component('DataTable', DataTable);
app.component('Column', Column);

app.mount('#app');